const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");


//Route for creating a course
router.post("/",auth.verify, (req,res) => {


	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin == true) {

		//courseController.addCourse(req.body) <= before
		courseController.addCourse(req.body).then(resultFromController => res.send(
		resultFromController))
	} else {
		return false
		//other solution: res.send ("Not an admin")
	}
}) 


//Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

	//()- this is a paramter
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

//Mini Activity
/*
	Create a route and controller for retrieving all active courses. No need to log in
	
	getAllActive

	"/"
*/


// Route for retrieving all the Active courses
router.get("/", (req, res) =>  {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController)) //this is the client or send it to postman

})


// Route for retrieving a specific course
// Url: localhost:4000/courses/651516371471741848
/*router.get("/:courseId", (res, req) => {
	console.log(req.params.courseId)

	//req.params - dynamic
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})*/

router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));

})


// Route for updating a course
// :courseId is the req.params
router.put("/:courseId", auth.verify, (req, res) => {

	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin, //.isAdmin is needed to know if it is an admin or not or it will give the whole payload like user, email, isAdmin
		updatedCourse: req.body //from reqbody, deets inside the rebody(postman)
	}

	courseController.updatedCourse(data).then(resultFromController => res.send(resultFromController))


}) 

// Route for archiving/deleting a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin, 
		archivedCourse: req.body
	}

	courseController.archivedCourse(data).then(resultFromController => res.send(resultFromController))


}) 




module.exports = router;
