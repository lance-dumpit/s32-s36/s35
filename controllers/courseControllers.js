const Course = require("../models/Course");
const auth = require("../auth");

// reqBody will be the information getting addcourse
module.exports.addCourse = (reqBody) => {


	let newCourse = new Course ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	// newCourse will be saved in the data 
	return newCourse.save().then((course, error) => {

		if (error) {

			return false
		} else {
			return course
		}
	})
}

//Retrieve all Courses
// in postman, authenticate or login first and then copy the token, paste it in /all token in /all(endpoint) then submit to retrieve all the courses 

module.exports.getAllCourses = () => {

	// ({}) no specific field
	return Course.find({}).then(result => {

		return result
	})
}

// Mini Activity
// Retrieve All Active Courses

module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {

		return result
	})
}

//Retrieve a specific course


module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


// Updating a Course
module.exports.updatedCourse = (data) => {
	console.log(data) //to know the info inside

	return Course.findById(data.courseId).then((result, error) => {

		console.log(result) //if there's a document, found document in gitbash

		// if user in admin
		// data from courseRoute
		if(data.isAdmin){
			//if true this will be reassign/update
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			console.log(result) //to see/check the updated document


			return result.save().then((updatedCourse, error) => {

				if(error){
					return false
				} else {
					return updatedCourse
				}
			})

		} else {
			return "Not Admin"
		}
	})
}

// Activity
// Archiving a course
module.exports.archivedCourse = (data) => {
	console.log(data)

	return Course.findById(data.courseId).then((result, error) => {

		if(data.isAdmin){
			result.isActive = false

			console.log(result)
			
			return result.save().then((archivedCourse, error) => {

				if(error){
					return false
				} else {
					return archivedCourse
				}
			})

		} else {
			return "Not Admin"
		}
	})
}